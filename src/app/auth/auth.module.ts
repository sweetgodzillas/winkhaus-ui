import { AuthRoutes } from './auth.routing';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    AuthRoutes
  ],
  declarations: [LoginComponent]
})
export class AuthModule { }
