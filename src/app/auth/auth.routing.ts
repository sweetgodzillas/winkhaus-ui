import { LoginComponent } from './login/login.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{ path: 'login', component: LoginComponent }];

export const AuthRoutes = RouterModule.forChild(routes);
